
$(document).ready(function () {
  $(document).bind('overlay_image_loaded', function (event, data) {
    OverlayComment.init(data);
  });
});

OverlayComment = {};

/**
 * Global variables needed for the slide effects.
 */
OverlayComment.closeInProgress = false;
OverlayComment.openInProgress = false;
OverlayComment.openQueue = null;

/**
 * Global variable that defines if any form element within the
 * comment widget is focused.
 */
OverlayComment.focus = false;

/**
 * Initialization of the comment widget.
 */
OverlayComment.init = function(data) {
  if ($('#overlay-image-comments').length == 0) {
    var img_comments = $('<div>');
    img_comments.attr('id', 'overlay-image-comments');
    $('#overlay-image-def').append(img_comments);

    /* For IE: */
    $('#overlay-image-comments').css('opacity', 0.8);

    /* Initialize the mouse hover handler */
    $('#overlay-image-comments').hover(OverlayComment.show, OverlayComment.hide);
  }
  OverlayComment.setComments(data);

};

/**
 * Set / renew the data in the comments widget.
 */
OverlayComment.setComments = function (data) {
  $('#overlay-image-comments').empty();
  $('#overlay-image-comments').css('width', '20px');
  if (!data.comments) {
    return;
  }
  $('#overlay-image-comments').html(data.comments);
  $('#overlay-gallery-comment-form').ajaxForm({
    dataType:     'json',
    success:      OverlayComment.postPost,
    beforeSubmit: OverlayComment.prePost
  });
  $('#overlay-gallery-comment-form').find('input, textarea')
  .blur(function(){
    OverlayComment.focus = false;
  })
  .focus(function() {
    OverlayComment.focus = true;
  });
};

/**
 * Actions to be performed before we fire the ajax event
 */
OverlayComment.prePost = function() {
  $('#overlay-image-comments').addClass('gallery-loading');
};

OverlayComment.postPost = function(data) {
  $('#overlay-image-comments').removeClass('gallery-loading');
  if (data.success) {
    /* insert comment with slide effect */
    var comment = $(data.comment);
    comment.css('display', 'none');
    $('#overlay-image-comments #comments .overlay-comments').prepend(comment);
    comment.slideDown(function () {
      /* update cache */
      OverlayImage.preloaded[OverlayImage.imageData.id].comments = $('#overlay-image-comments').html();
    });
  } else {
    alert(data.errors);
  }
  $('#overlay-gallery-comment-form input:submit').blur();
};

OverlayComment.show = function(item) {
  OverlayComment.openQueue = null;
  if (OverlayComment.closeInProgress || OverlayComment.openInProgress) {
    return;
  }
  OverlayComment.openInProgress = true;
  var comments = $(this).children('#comments');
  if (comments.length > 0) {
    comments.show();
  }
  $(this).animate({
    width: '40%'
  }, 'slow', function () {
    OverlayComment.openInProgress = false;
    if (typeof(OverlayComment.openQueue) == 'function') {
      OverlayComment.openQueue();
      OverlayComment.openQueue = null;
    }
  });
};

OverlayComment.hide = function(item) {
  /* don't hide if an element has the focus */
  if (OverlayComment.focus || OverlayComment.closeInProgress) {
    return;
  }
  if (OverlayComment.openInProgress) {
    OverlayComment.openQueue = OverlayComment.hide;
    return;
  }
  var container = $('#overlay-image-comments');
  OverlayComment.closeInProgress = true;
  var comments = $(this).children('#comments');
  container.animate({
    width: '20px'
  }, 'slow', function () {
    OverlayComment.closeInProgress = false;
    var comments = $(this).children('#comments');
    if (comments.length > 0) {
      comments.hide();
    }
  });
};