/* Variables */
OverlayGallery = {};
OverlayGallery.gallery = {};
OverlayGallery.state = {};

$(document).ready(function () {
  OverlayGallery.init();
});

OverlayGallery.init = function () {
  $('a.overlay-gallery').click(OverlayGallery.open);
  /* Escape key handler */
  $(document).keyup(function(e) {
    if (e.which == 27 && OverlayGallery.state == 'open') {
      OverlayGallery.close();
    }
  });
};

/**
 *  Loads the background divs for the gallery
 */
OverlayGallery.loadBG = function () {
  $('body').append('<div id="overlay-gallery-bg"></div>');
  $('#overlay-gallery-bg').css('opacity', .1).fadeTo('normal',0.8);
  $('body').append('<div id="overlay-gallery-container"></div>');
  $('#overlay-gallery-container').append('<div id="overlay-gallery-header"></div>');
  $('#overlay-gallery-container').append('<div id="overlay-gallery-close" title="'+Drupal.t('Close Album')+'"></div>');
  $('#overlay-gallery-close').click(OverlayGallery.close);
};

/**
 * Opens the Gallery.
 * @param {Object} event
 */
OverlayGallery.open = function (event) {
  if (event.ctrlKey || event.shiftKey) {
    self.location = $(this).attr('href');
    return false;
  }
  scroll(0,0);
  if (OverlayGallery.state != 'open') {
    OverlayGallery.state = 'open';
    
    /* No Overlay Gallery for IE6 */
    if ($.browser.msie && $.browser.version.substr(0, 1) == '6') {
      return true;
    }
    OverlayGallery.loadBG();
    OverlayGallery.loadAlbum($(this).attr('rel'));
  }
  return false;
};

/**
 * Loads the album with the given id
 * @param {Object} id
 *   The id of the Drupal object (nid, tid,...)
 */
OverlayGallery.loadAlbum = function(id) {
  $('#overlay-gallery-container').addClass('gallery-loading');
  $.getJSON(Drupal.settings.basePath+'overlay_gallery_load/'+id, OverlayGallery.displayAlbum);
};

/**
 * This is the success handler for the JSON call. It displays the 
 * gallery returned by getJSON().
 * @param {Object} data
 *   The gallery data returned by the ajax json-call.
 */
OverlayGallery.displayAlbum = function(data) {
  $('#overlay-gallery-container').removeClass('gallery-loading');
  OverlayGallery.gallery = data.images;
  var container = $('#overlay-gallery-container');

  /* gallery title and description first */
  $('#overlay-gallery-header').append('<h1>'+data.title+'</h1>');
  $('#overlay-gallery-header').append('<div id="overlay-gallery-play"><a href="#">'+Drupal.t('Slideshow')+'</a></div>');

  /* Slideshow link for this gallery */
  $('#overlay-gallery-play a').click(function () {
    OverlayImage.image = 0;
    OverlayImage.showImage(OverlayGallery.startSlideshow);
    return false;
  });

  /* finally we display the thumbnails */
  container.append('<div id="overlay-thumbs-container"></div>');
  $('#overlay-thumbs-panel').append('<div id="overlay-start-slideshow"></div>');
  $(data.images).each(OverlayGallery.insertThumbnail);

  /* Initialize the thumbnail click handler */
  OverlayImage.init();
};

/**
 * Inserts a single thumbnail into the gallery container
 * @param {Object} image
 */
OverlayGallery.insertThumbnail = function (image) {
  var img = $(document.createElement('img'));
  var img_link = $(document.createElement('a'));
  var img_definition = $(document.createElement('span'));
  var img_container =  $(document.createElement('div'));

  img.attr('src', this.thumbnail);
  img_link.attr({
    'href': this.normal,
    'class': 'gallery-thumb-link overlay-thumb',
    'rel': image
  }).append(img);
  img_definition.attr('class', 'gallery-thumb-def').append(img_link);
  img_container.attr('class', 'gallery-thumb-container').append(img_definition);
  $('#overlay-thumbs-container').append(img_container);
  img_container.fadeIn();
};

OverlayGallery.close = function (event) {
  OverlayGallery.state = 'closed';
  OverlayImage.state = 'closed';
  $('#overlay-gallery-container').fadeOut('fast', function () {
    $('#overlay-gallery-container').remove();
    $('#overlay-gallery-bg').fadeOut('fast', function() {
      $('#overlay-gallery-bg').remove();
    });
  });
  OverlayGallery.gallery = {};
  OverlayGallery.stopSlideshow();
  return false;
};

OverlayGallery.startSlideshow = function () {
  $('#oi_pause').show();
  $('#oi_play').hide();
  $(document).everyTime('5s', 'play-slideshow', function () {
    OverlayImage.showNext(null);
  });
};

OverlayGallery.stopSlideshow = function () {
  $('#oi_play').show();
  $('#oi_pause').hide();
  $(document).stopTime('play-slideshow');
};
