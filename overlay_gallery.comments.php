<?php

/**
 * Renders the comments widget shown on the overlay image.
 * This can only be used if the gallery images are nodes
 * of course.
 * @param $node
 *  The image node
 * @return string
 *  Rendered comments widget
 */
function theme_overlay_comments($node) {
  $content = '';
  if ($node->comment == 0) {
    return '';
  }
  $content .= '<div id="comments">';
  $content .= '<h2 class="comments">'. t('Comments') .'</h2>';

  /* render the add comment form */
  if ($node->comment == 2 && user_access('post comments')) {
    $content .= '<h2 class="comment">'. t('Add comment') .'</h2>';
    $content .= drupal_get_form('overlay_gallery_comment_form', $node);
  } else {
    $content .= '<h2 class="comment">'. t('Login or register to post comments!') .'</h2>';
  }

  /* render the comments */
  $content .= '<div class="overlay-comments">';
  $query = '
    SELECT       c.cid as cid,
                 c.pid,
                 c.nid,
                 c.subject,
                 c.comment,
                 c.format,
                 c.timestamp,
                 c.name,
                 c.mail,
                 c.homepage,
                 u.uid,
                 u.name AS registered_name,
                 u.signature,
                 u.signature_format,
                 u.picture,
                 u.data,
                 c.thread,
                 c.status 
    FROM         {comments} c 
    INNER JOIN   {users} u ON c.uid = u.uid 
    WHERE        c.nid = %d
    AND          c.status = %d
    ORDER BY     c.cid DESC
  ';
  $query = db_rewrite_sql($query, 'c', 'cid');
  $result = db_query($query, $node->nid);
  while ($comment = db_fetch_object($result)) {
    $comment = drupal_unpack($comment); 
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    $content .= theme('comment_view', $comment, $node);
  }
  $content .= '</div>';
  $content .= '</div>';
  return $content;
}

/**
 * Simplified copy of comment_form().
 * @param $form_state
 * @param $node
 * @return array
 *  The form
 */
function overlay_gallery_comment_form($form_state, $node) {
  global $user;
  $form['#attributes'] = array('class' => 'box');
  if ($user->uid) {
    $form['_author'] = array(
      '#type' => 'item',
      '#title' => t('Your name'),
      '#value' => theme('username', $user)
    );
    $form['author'] = array(
      '#type' => 'value',
      '#value' => $user->name
    );
  } else {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#maxlength' => 60,
      '#size' => 30,
      '#default_value' => variable_get('anonymous', t('Anonymous'))
    );
    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#maxlength' => 64,
      '#size' => 30,
      '#description' => t('The content of this field is kept private and will not be shown publicly.')
    );
  }
  if (variable_get('comment_anonymous_'. $node->type, COMMENT_ANONYMOUS_MAYNOT_CONTACT) == COMMENT_ANONYMOUS_MUST_CONTACT) {
    $form['name']['#required'] = TRUE;
    $form['mail']['#required'] = TRUE;
  }

  if (variable_get('comment_subject_field_'. $node->type, 1) == 1) {
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#maxlength' => 64,
    );
  }
  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
    '#rows' => 15,
    '#required' => TRUE,
  );
  $form['cid'] = array('#type' => 'value', '#value' => NULL);
  $form['pid'] = array('#type' => 'value', '#value' => NULL);
  $form['uid'] = array('#type' => 'value', '#value' => 0);
  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);

  $form['#token'] = 'comment'. $node->nid;
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'), '#weight' => 19);
  return $form;
}

/**
 * Possible errors should be returned in the JSON response
 * and the error message queue needs to be cleaned so that
 * the error masseges will not be shown upon a page reload
 * anymore.
 * @param $form
 * @param $form_state
 */
function overlay_gallery_comment_form_validate($form, &$form_state) {
  $errors = form_get_errors();
  if (!empty($errors)) {
    drupal_get_messages('error');
    $result = array(
      'success' => FALSE,
      'errors' => implode("\n", $errors),
    );
    drupal_json($result);
    exit;
  }
}

/**
 * 
 * @param $form
 * @param $form_state
 * @return unknown_type
 */
function overlay_gallery_comment_form_submit($form, &$form_state) {
  $form_state['values']['format'] = FILTER_FORMAT_DEFAULT;
  _comment_form_submit($form_state['values']);
  if ($cid = comment_save($form_state['values'])) {
    $node = node_load($form_state['values']['nid']);
    $query = 'SELECT c.cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp, c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature, u.signature_format, u.picture, u.data, c.status FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE c.cid = %d AND c.status = %d';
    $query = db_rewrite_sql($query, 'c', 'cid');
    $comment = db_fetch_object(db_query($query, $cid, COMMENT_PUBLISHED));
    if ($comment) {
      /* Successfully saved the comment.*/
      /* The comment is returned so that it can be inserted by the script */
      $result = array(
        'success' => TRUE,
        'comment' => theme('comment_view', $comment, $node),
      );
    } elseif($cid) {
      /* Most likely the user was anonymous and the comment is in the approval queue. */
      $comment = '';
      $msgs = drupal_get_messages('status');
      foreach ($msgs as $msg) {
        $comment .= implode("<br />", $msg)."<br />";
      }
      $result = array(
        'success' => TRUE,
        'comment' => '<h2>'. $comment .'</h2>',
      );
    }
    
  } else {
    $result = array(
      'success' => FALSE,
      'comment' => t('An unexpected error occured while trying to save your comment - sorry!'),
    );
  }
  drupal_json($result);
  exit;
}